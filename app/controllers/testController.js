const express = require('express');
const router = express.Router();
const TestService = require('../services/testService');

router.get('/', async (req, res) => {
  try {
    const result = await TestService.test();
    res.status(200).json(result);
  } catch (error) {
    console.log(error);
  }
});

router.get('/gg', async (req, res) => {
  try {
    //   const result = await TestService.test();
    res.status(200).json('csdfsdfsdf');
  } catch (error) {
    console.log(error);
  }
});

router.post("/test2", async (req, res, next) => {
  try {
    const { username, password } = req.body;
    const result = await TestService.Login(username, password, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post("/register", async (req, res, next) => {
  try {
    const { username, password, email, company, name, surname, companyAddress, tel, headNo } = req.body;
    const result = await TestService.Register(username, password, email, company, name, surname, companyAddress, tel, headNo, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.get("/company", async (req, res, next) => {
  try {
    // const { username, password, email, company, name, surname, companyAddress, tel, headNo } = req.body;
    const result = await TestService.Company(next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.get("/companyw", async (req, res, next) => {
  try {
    // const { username, password, email, company, name, surname, companyAddress, tel, headNo } = req.body;
    const result = await TestService.WaitCompany(next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post("/companye", async (req, res, next) => {
  try {
    const { company, companyAddress, companyId, name, surname, tel, headNo, adminId } = req.body;
    const result = await TestService.EditCompany(company, companyAddress, companyId, name, surname, tel, headNo, adminId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post("/companyes", async (req, res, next) => {
  try {
    const { maxAdmin, maxUser, status, companyId, adminId } = req.body;
    const result = await TestService.EditCompanyStatus(maxAdmin, maxUser, status, companyId, adminId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post("/companyr", async (req, res, next) => {
  try {
    const { status, companyId, adminId } = req.body;
    const result = await TestService.RemoveCompany(status, companyId, adminId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.get("/ncompany", async (req, res, next) => {
  try {
    const result = await TestService.CountCompany(next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.get("/ncompanyw", async (req, res, next) => {
  try {
    const result = await TestService.CountWaitCompany(next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post("/companyc", async (req, res, next) => {
  try {
    const { status, companyId, adminId } = req.body;
    const result = await TestService.ConfirmCompany(status, companyId, adminId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

module.exports = router;
