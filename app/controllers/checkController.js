const express = require('express');
const router = express.Router();
const CheckService = require('../services/checkService');

router.post('/add', async (req, res, next) => {
  try {
    const { matId, num, date, userId, compId, matNum } = req.body;
    const result = await CheckService.AddCheck(
      matId,
      num,
      date,
      userId,
      compId,
      matNum,
      next
    );
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/return/add', async (req, res, next) => {
  try {
    const { matId, num, date, userId, compId, matNum } = req.body;
    const result = await CheckService.AddReturn(
      matId,
      num,
      date,
      userId,
      compId,
      matNum,
      next
    );
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/gets/date', async (req, res, next) => {
  try {
    const { matId } = req.body;
    const result = await CheckService.RecentInfoByMat(matId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

module.exports = router;
