const express = require('express');
const router = express.Router();
const TestService = require('../services/testService');

router.post('/listware', async (req, res, next) => {
  try {
    const { adminId } = req.body;
    const result = await TestService.WareList(adminId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/liststore', async (req, res, next) => {
  try {
    const { ware } = req.body;
    const result = await TestService.StoreList(ware, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/newmat', async (req, res, next) => {
  try {
    const { name, sup, store, num, price, date, unit, compId } = req.body;
    const result = await TestService.NewMaterial(name, sup, store, num, price, date, unit, compId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/newstore', async (req, res, next) => {
  try {
    const { qr, mat, num, date, userId, compId } = req.body;
    const result = await TestService.NewStorage(qr, mat, num, date, userId, compId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

//MAT
router.post('/listoldmat', async (req, res, next) => {
  try {
    const { adminId } = req.body;
    const result = await TestService.OldMatList(adminId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/readqrmat', async (req, res, next) => {
  try {
    const { name } = req.body;
    const result = await TestService.ReadMatQr(name, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

module.exports = router;
