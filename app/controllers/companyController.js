const express = require('express');
const router = express.Router();
const TestService = require('../services/testService');
const CompanyService = require('../services/companyService');

router.post('/', async (req, res, next) => {
  try {
    const { compId } = req.body;
    const result = await CompanyService.CompanyById(compId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.get('/company', async (req, res, next) => {
  try {
    const result = await TestService.Company(next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.get('/companyw', async (req, res, next) => {
  try {
    const result = await TestService.WaitCompany(next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/companye', async (req, res, next) => {
  try {
    const { company, companyAddress, companyId } = req.body;
    const result = await TestService.EditCompany(
      company,
      companyAddress,
      companyId,
      next
    );
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/companyes', async (req, res, next) => {
  try {
    const { maxAdmin, maxUser, status, companyId } = req.body;
    const result = await TestService.EditCompanyStatus(
      maxAdmin,
      maxUser,
      status,
      companyId,
      next
    );
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/companyr', async (req, res, next) => {
  try {
    const { status, companyId, email, note, arr } = req.body;
    const result = await TestService.RemoveCompany(
      status,
      companyId,
      email,
      note,
      arr,
      next
    );
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/companyj', async (req, res, next) => {
  try {
    const { status, companyId, email, note, name, adminId } = req.body;
    const result = await TestService.RejectCompany(
      status,
      companyId,
      email,
      note,
      name,
      adminId,
      next
    );
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.get('/ncompany', async (req, res, next) => {
  try {
    const result = await TestService.CountCompany(next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.get('/ncompanyw', async (req, res, next) => {
  try {
    const result = await TestService.CountWaitCompany(next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/companyc', async (req, res, next) => {
  try {
    const { status, companyId, adminId } = req.body;
    const result = await TestService.ConfirmCompany(
      status,
      companyId,
      adminId,
      next
    );
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

module.exports = router;
