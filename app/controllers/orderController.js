const express = require('express');
const router = express.Router();
const OrderService = require('../services/orderService');

router.post('/add', async (req, res, next) => {
  try {
    const {arr, date, userId, compId } = req.body;
    const result = await OrderService.NewOrder(arr, date, userId, compId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/', async (req, res, next) => {
  try {
    const { userId } = req.body;
    const result = await OrderService.ListByUser(userId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/list', async (req, res, next) => {
  try {
    const { orderId } = req.body;
    const result = await OrderService.GoodsList(orderId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/updates/status', async (req, res, next) => {
  try {
    const { orderId, status } = req.body;
    const result = await OrderService.Status(orderId, status, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/admin', async (req, res, next) => {
  try {
    const { adminId } = req.body;
    const result = await OrderService.ListByAdmin(adminId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

module.exports = router;
