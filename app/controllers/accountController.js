const express = require('express');
const router = express.Router();
const TestService = require('../services/testService');
const AccountService = require('../services/accountService');

router.post('/login', async (req, res, next) => {
  try {
    const { username, password } = req.body;
    const result = await TestService.Login2(username, password, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/register', async (req, res, next) => {
  try {
    const {
      username,
      password,
      email,
      company,
      name,
      surname,
      companyAddress,
      tel,
      headNo,
    } = req.body;
    const result = await TestService.Register(
      username,
      password,
      email,
      company,
      name,
      surname,
      companyAddress,
      tel,
      headNo,
      next
    );
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/forgot', async (req, res, next) => {
  try {
    const { email, username } = req.body;
    const result = await AccountService.Forgot(email, username, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/reset', async (req, res, next) => {
  try {
    const { id, pass } = req.body;
    const result = await AccountService.ResetPass(id, pass, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

module.exports = router;
