const express = require('express');
const router = express.Router();
const SupplierService = require('../services/supplierService');

router.post('/info', async (req, res, next) => {
  try {
    const { matId } = req.body;
    const result = await SupplierService.InfoListByMat(matId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

module.exports = router;
