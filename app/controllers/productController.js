const express = require('express');
const router = express.Router();
const ProductService = require('../services/productService');

router.post('/cates/list', async (req, res, next) => {
  try {
    const { compId } = req.body;
    const result = await ProductService.CateList(compId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/types/list', async (req, res, next) => {
  try {
    const { cateId } = req.body;
    const result = await ProductService.TypeList(cateId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/type', async (req, res, next) => {
  try {
    const { compId } = req.body;
    const result = await ProductService.TypeListByComp(compId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/list', async (req, res, next) => {
  try {
    const { compId, typeId } = req.body;
    const result = await ProductService.ListByType(compId, typeId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/goods', async (req, res, next) => {
  try {
    const { proId } = req.body;
    const result = await ProductService.GoodsByPro(proId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/goods/add', async (req, res, next) => {
  try {
    const { arr, proId } = req.body;
    const result = await ProductService.AddGoods(arr, proId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/cates/add', async (req, res, next) => {
  try {
    const { arr, compId } = req.body;
    const result = await ProductService.AddCate(arr, compId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/types/add', async (req, res, next) => {
  try {
    const { arr, compId } = req.body;
    const result = await ProductService.AddType(arr, compId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/cates/edit', async (req, res, next) => {
  try {
    const { name, desc, cateId } = req.body;
    const result = await ProductService.EditCate(name, desc, cateId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/types/edit', async (req, res, next) => {
  try {
    const { name, desc, typeId } = req.body;
    const result = await ProductService.EditType(name, desc, typeId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/cates/del', async (req, res, next) => {
  try {
    const { arr } = req.body;
    const result = await ProductService.DeleteCate(arr, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/types/del', async (req, res, next) => {
  try {
    const { selected } = req.body;
    const result = await ProductService.DeleteType(selected, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/goods/del', async (req, res, next) => {
  try {
    const { arr, proId } = req.body;
    const result = await ProductService.DelGoods(arr, proId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/lists/comp', async (req, res, next) => {
  try {
    const { compId } = req.body;
    const result = await ProductService.ListByComp(compId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

module.exports = router;
