const express = require('express');
const router = express.Router();
const TestService = require('../services/testService');

router.post('/admin', async (req, res, next) => {
  try {
    const { compId } = req.body;
    const result = await TestService.Admin(compId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/user', async (req, res, next) => {
  try {
    const { compId } = req.body;
    const result = await TestService.User(compId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.get('/companyw', async (req, res, next) => {
  try {
    const result = await TestService.WaitCompany(next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/admine', async (req, res, next) => {
  try {
    const { name, surname, tel, headNo, adminId } = req.body;
    const result = await TestService.EditAdmin(
      name,
      surname,
      tel,
      adminId,
      next
    );
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/admines', async (req, res, next) => {
  try {
    const { status, adminId } = req.body;
    const result = await TestService.EditAdminStatus(status, adminId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

// router.post('/adminr', async (req, res, next) => {
//   try {
//     const { status, companyId, adminId } = req.body;
//     const result = await TestService.RemoveAdmin(
//       status,
//       adminId,
//       next
//     );
//     res.status(200).json(result);
//   } catch (error) {
//     next(error);
//   }
// });

router.post('/adminr', async (req, res, next) => {
  try {
    const { status, arr } = req.body;
    console.log(arr);
    const result = await TestService.RemoveAdmin(
      status,
      arr,
      next
    );
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/usere', async (req, res, next) => {
  try {
    const { name, surname, tel, userId } = req.body;
    const result = await TestService.EditUser(
      name,
      surname,
      tel,
      userId,
      next
    );
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/useres', async (req, res, next) => {
  try {
    const { status, userId } = req.body;
    const result = await TestService.EditUserStatus(status, userId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/userr', async (req, res, next) => {
  try {
    const { status, arr } = req.body;
    const result = await TestService.RemoveUser(
      status,
      arr,
      next
    );
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/companyr', async (req, res, next) => {
  try {
    const { status, companyId, adminId } = req.body;
    const result = await TestService.RemoveCompany(
      status,
      companyId,
      adminId,
      next
    );
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.get('/ncompany', async (req, res, next) => {
  try {
    const result = await TestService.CountCompany(next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.get('/ncompanyw', async (req, res, next) => {
  try {
    const result = await TestService.CountWaitCompany(next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/companyc', async (req, res, next) => {
  try {
    const { status, companyId, adminId } = req.body;
    const result = await TestService.ConfirmCompany(
      status,
      companyId,
      adminId,
      next
    );
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

module.exports = router;
