const express = require("express");
const router = express.Router();
const PassportJWT = require("../middlewares/PassportJWT");
//import validation req
const { body, validationResult } = require("express-validator");
//import service
const ExampleService = require("../services/exampleService");

router.get("/", [PassportJWT.isLogin], async (req, res, next) => {
  try {
    const result = await ExampleService.GetExample(next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});
router.post(
  "/login",
  [
    body("username").not().isEmpty().withMessage("Please enter username field"),
    body("password").not().isEmpty().withMessage("Please enter password field"),
  ],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const error = new Error("The information received is not correct.");
        error.statusCode = 422;
        error.validation = errors.array();
        throw error;
      }
      const result = await ExampleService.LoginExample(req, next);
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  }
);

module.exports = router;
