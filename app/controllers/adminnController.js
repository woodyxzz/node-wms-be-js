const express = require('express');
const router = express.Router();
const adminnService = require('../services/adminnService');

// List
router.get('/listuser', async (req, res, next) => {
  try {
    const result = await adminnService.listuser(next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.get('/listware', async (req, res, next) => {
  try {
    const result = await adminnService.listware(next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.get('/listunit', async (req, res, next) => {
  try {
    const result = await adminnService.listunit(next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/liststorage', async (req, res, next) => {
  try {
    const { idWare } = req.body;
    const result = await adminnService.liststorage(idWare, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.get('/listcountrec', async (req, res, next) => {
  try {
    const result = await adminnService.listcountrec(next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});
router.get('/listcountrev', async (req, res, next) => {
  try {
    const result = await adminnService.listcountrev(next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/listnameware', async (req, res, next) => {
  try {
    const { idWare } = req.body;
    const result = await adminnService.listnameware(idWare, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/listprofile', async (req, res, next) => {
  try {
    const { id } = req.body;
    const result = await adminnService.listprofile(id, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/showproducts', async (req, res, next) => {
  try {
    const { typeid } = req.body;
    console.log('typeid', typeid);
    let result = await adminnService.showproducts(typeid, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/showmaterials', async (req, res, next) => {
  try {
    const { typeid, typeidlast, comp } = req.body;
    console.log('typeid and typeidlast', typeid, typeidlast, comp);
    let result = await adminnService.showmaterials(
      typeid,
      typeidlast,
      comp,
      next
    );
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/showcarrymaterials', async (req, res, next) => {
  try {
    const { matid, matidlast, comp } = req.body;
    console.log('typeid and typeidlast', matid, matidlast, comp);
    let result = await adminnService.showcarrymaterials(
      matid,
      matidlast,
      comp,
      next
    );
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/showreceiptindate', async (req, res, next) => {
  try {
    const { recId, comp } = req.body;
    console.log('date', recId, comp);
    let result = await adminnService.showreceiptindate(recId, comp, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/showidreturn', async (req, res, next) => {
  try {
    const { retId, comp } = req.body;
    console.log('date', retId, comp);
    let result = await adminnService.showidreturn(retId, comp, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/searchreveal', async (req, res, next) => {
  try {
    const { revId, comp } = req.body;
    console.log('date', revId, comp);
    let result = await adminnService.searchreveal(revId, comp, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/showcheck', async (req, res, next) => {
  try {
    const { chId, comp } = req.body;
    console.log('date', chId, comp);
    let result = await adminnService.showcheck(chId, comp, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/searchcarry', async (req, res, next) => {
  try {
    const { carId, comp } = req.body;
    console.log('date', carId, comp);
    let result = await adminnService.searchcarry(carId, comp, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.get('/selectunit', async (req, res, next) => {
  try {
    const result = await adminnService.selectunit(next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

// Add
router.post('/addstorages', async (req, res, next) => {
  try {
    const { addstorages, comp } = req.body;
    console.log('test', addstorages, comp);
    const result = await adminnService.addstorage(addstorages, comp, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/addunits', async (req, res, next) => {
  try {
    const { addun, comp } = req.body;
    console.log('test', addun);
    const result = await adminnService.addunits(addun, comp, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/addcarrys', async (req, res, next) => {
  try {
    const { materialsList, adminId, comp } = req.body;
    console.log('test', materialsList, adminId, comp);
    const result = await adminnService.addcarrys(
      materialsList,
      adminId,
      comp,
      next
    );
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/adduserfile', async (req, res, next) => {
  try {
    const { excelData, adminid, comp } = req.body;
    // console.log('test', excelData);
    const result = await adminnService.adduserfile(
      excelData,
      adminid,
      comp,
      next
    );
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/addproduct', async (req, res, next) => {
  try {
    const { name, brand, color, desc, protid, unit, comp } = req.body;
    console.log('test', name, brand, color, desc, protid, unit, comp);
    const result = await adminnService.addproduct(
      name,
      brand,
      color,
      desc,
      protid,
      unit,
      comp,
      next
    );
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/adduser', async (req, res, next) => {
  try {
    const {
      username,
      password,
      name,
      surname,
      email,
      tel,
      userNo,
      dep,
      adminId,
      comp,
    } = req.body;
    const result = await adminnService.adduser(
      username,
      password,
      name,
      surname,
      email,
      tel,
      userNo,
      dep,
      adminId,
      comp,
      next
    );
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/addware', async (req, res, next) => {
  try {
    const { name, desc, comp } = req.body;
    const result = await adminnService.addware(name, desc, comp, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

// Edit
router.post('/editunit', async (req, res, next) => {
  try {
    const { unitid, name, desc } = req.body;
    console.log('cos Ok', name, desc);
    const result = await adminnService.editunit(unitid, name, desc, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/editstorages', async (req, res, next) => {
  try {
    const { storeid, name, desc } = req.body;
    console.log('contoller', storeid, name, desc);
    const result = await adminnService.editstorage(storeid, name, desc, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/edituser', async (req, res, next) => {
  try {
    const { userid, name, surname, email, tel, dep } = req.body;
    const result = await adminnService.edituser(
      userid,
      name,
      surname,
      email,
      tel,
      dep,
      next
    );
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/editware', async (req, res, next) => {
  try {
    const { wareid, name, desc } = req.body;
    const result = await adminnService.editware(wareid, name, desc, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/editproduct', async (req, res, next) => {
  try {
    const { prodid, name, brand, color, desc, protid, unit } = req.body;
    const result = await adminnService.editproduct(
      prodid,
      name,
      brand,
      color,
      desc,
      protid,
      unit,
      next
    );
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

// Delete
router.post('/deletestorageall', async (req, res, next) => {
  try {
    const { selected } = req.body;
    console.log('contoller', selected);
    const result = await adminnService.deletestorageall(selected, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/deletetuserall', async (req, res, next) => {
  try {
    const { selected } = req.body;
    const result = await adminnService.deletetuserall(selected, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/deletewareall', async (req, res, next) => {
  try {
    const { selected } = req.body;
    const result = await adminnService.deletewareall(selected, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/deleteproductall', async (req, res, next) => {
  try {
    const { selected } = req.body;
    const result = await adminnService.deleteproductall(selected, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/deleteunitall', async (req, res, next) => {
  try {
    const { selected } = req.body;
    const result = await adminnService.deleteunitall(selected, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

// Search
router.post('/searchproducts', async (req, res, next) => {
  try {
    const { compid } = req.body;
    console.log('compid', compid);
    let result = await adminnService.searchproducts(compid, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/searchidrecipt', async (req, res, next) => {
  try {
    const { comp } = req.body;
    console.log('in', comp);
    let result = await adminnService.searchidrecipt(comp, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/searchidreturn', async (req, res, next) => {
  try {
    const { comp } = req.body;
    console.log('in', comp);
    let result = await adminnService.searchidreturn(comp, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/searchidreveal', async (req, res, next) => {
  try {
    const { comp } = req.body;
    console.log('in', comp);
    let result = await adminnService.searchidreveal(comp, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/searchidcheck', async (req, res, next) => {
  try {
    const { comp } = req.body;
    console.log('in', comp);
    let result = await adminnService.searchidcheck(comp, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/searchidcarry', async (req, res, next) => {
  try {
    const { comp } = req.body;
    console.log('in', comp);
    let result = await adminnService.searchidcarry(comp, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/searchmaterials', async (req, res, next) => {
  try {
    const { compid } = req.body;
    console.log('compid', compid);
    let result = await adminnService.searchmaterials(compid, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/newidcarry', async (req, res, next) => {
  try {
    const { compid } = req.body;
    console.log('compid', compid);
    let result = await adminnService.newidcarry(compid, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

module.exports = router;
