const express = require('express');
const router = express.Router();
const StorageService = require('../services/storageService');

router.post('/info', async (req, res, next) => {
  try {
    const { matId } = req.body;
    const result = await StorageService.InfoListByMat(matId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/infos/store', async (req, res, next) => {
  try {
    const { name } = req.body;
    const result = await StorageService.InfoListByMat(name, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

module.exports = router;
