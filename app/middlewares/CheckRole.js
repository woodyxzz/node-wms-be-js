module.exports.isAdmin = (req, res, next) => {
  const { Role } = req.user.recordset[0];
  if (Role === "Admin") {
    next();
  } else {
    res.status(403).json({
      status_code: 403,
      message:
        "คุณไม่ได้รับอนุญาตให้ใช้งานส่วนนี้เพราะสามารถใช้ได้เฉพาะผู้ดูแลระบบเท่านั้น",
    });
  }
};

module.exports.isUser = (req, res, next) => {
  const { Role } = req.user.recordset[0];
  if (Role === "User" || Role === "Admin") {
    next();
  } else {
    res.status(403).json({
      status_code: 403,
      message:
        "คุณไม่ได้รับอนุญาตให้ใช้งานส่วนนี้เพราะสามารถใช้ได้เฉพาะผู้ใช้และผู้ดูแลระบบเท่านั้น",
    });
  }
};
