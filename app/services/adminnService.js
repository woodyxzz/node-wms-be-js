const e = require('cors');
const config = require('../configs/index');
const Userdb = require('../sql/Adminn/index');
const generator = require('generate-password');
const poolConnect = config.SQLCONNECTION.connect();
const moment = require('moment');
config.SQLCONNECTION.on('error', (err) => {
  console.log(err);
});
// List
exports.listuser = (next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(Userdb.getListUser());
      if (data.recordset) {
        resolve(data.recordset);
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.listware = (next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(Userdb.getListWare());
      if (data.recordset) {
        resolve(data.recordset);
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.listunit = (next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(Userdb.getListUnit());
      if (data.recordset) {
        resolve(data.recordset);
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.listcountrec = (next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(Userdb.getListCountRec());
      if (data.recordset) {
        resolve(data.recordset);
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.listcountrev = (next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(Userdb.getListCountRev());
      if (data.recordset) {
        resolve(data.recordset);
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};
exports.liststorage = (idWare, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(Userdb.postListStorage(idWare));
      if (data.recordset) {
        resolve(data.recordset);
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.listnameware = (idWare, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(Userdb.postNameWare(idWare));
      if (data.recordset) {
        resolve(data.recordset);
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.listprofile = (id, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(Userdb.postListProfile(id));
      if (data.recordset) {
        resolve(data.recordset);
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};
exports.showproducts = (typeid, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(Userdb.postShowProducts(typeid));
      if (data.recordset) {
        resolve(data.recordset);
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.showmaterials = (typeid, typeidlast, comp, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let first = typeid;
      let last = typeidlast;
      if (typeid.localeCompare(typeidlast) === 1) {
        first = typeidlast;
        last = typeid;
      }
      let data = await sqlRequest.query(
        Userdb.postShowMaterials(first, last, comp)
      );
      if (data.recordset) {
        resolve(data.recordset);
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.showcarrymaterials = (matid, matidlast, comp, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let first = matid;
      let last = matidlast;
      if (matid.localeCompare(matidlast) === 1) {
        first = matidlast;
        last = matid;
      }
      let data = await sqlRequest.query(
        Userdb.postShowCarryMaterials(first, last, comp)
      );
      if (data.recordset) {
        resolve(data.recordset);
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.showreceiptindate = (recId, comp, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(
        Userdb.postShowReceiptInDate(recId, comp)
      );
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('ไม่มีเลขที่เอกสารที่ท่านเลือก');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.showidreturn = (retId, comp, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(Userdb.postShowIdReturn(retId, comp));
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('ไม่มีเลขเอกสารนี้');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.searchreveal = (revId, comp, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(Userdb.postSearchReveal(revId, comp));
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('ไม่มีข้อมูลวันที่ระบุไว้');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.showcheck = (chId, comp, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(Userdb.postShowCheck(chId, comp));
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('ไม่มีข้อมูลวันที่ระบุไว้');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.searchcarry = (carId, comp, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(Userdb.postSearchCarry(carId, comp));
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('ไม่มีข้อมูลวันที่ระบุไว้');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.selectunit = (next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(Userdb.getListSelectUnit());
      if (data.recordset) {
        resolve(data.recordset);
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

// Add
exports.adduser = (
  username,
  password,
  name,
  surname,
  email,
  tel,
  userNo,
  dep,
  adminId,
  comp,
  next
) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let checkId = await sqlRequest.query(
        Userdb.checkIdUsername(username, userNo)
      );
      let checkIdAd = await sqlRequest.query(Userdb.checkIdAdmin(username));

      if (!checkId.recordset.length && !checkIdAd.recordset.length) {
        let prefix = '000';
        let prefix2 = '0000';
        let x = prefix + parseInt(comp.substring(2, 6));
        let str = x.substring(x.length - prefix.length);
        let dataId = await sqlRequest.query(Userdb.getRecentUserByComp(str));
        let recUser = dataId.recordset.length
          ? dataId.recordset[0].User_Id
          : '1';
        let y = dataId.recordset.length
          ? prefix2 + (parseInt(recUser.substring(5)) + 1)
          : prefix2 + parseInt(recUser);
        let str2 = y.substring(y.length - prefix2.length);
        let id = str + str2;
        let data = await sqlRequest.query(
          Userdb.postAddUser(
            username,
            password,
            name,
            surname,
            email,
            tel,
            userNo,
            dep,
            adminId,
            id
          )
        );
        if (data.rowsAffected) {
          resolve('OK');
        } else {
          const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
          error.statusCode = 401;
          throw error;
        }
      } else {
        const error = new Error('ชื่อผู้ใช้หรือเลขบัตรประชาชนถูกใช้แล้ว');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.addstorage = (addstorages, comp, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let prefix = '000';
      let prefix2 = '0000';
      let n = false;
      let x = prefix + parseInt(comp.substring(2, 6));
      let str = x.substring(x.length - prefix.length);
      let dataId = await sqlRequest.query(Userdb.getRecentStoreIdByWare(str));
      let recStore = dataId.recordset.length
        ? dataId.recordset[0].Store_Id
        : '1';
      for (let index = 0; index < addstorages.length; index++) {
        let y = dataId.recordset.length
          ? prefix2 + (parseInt(recStore.substring(5)) + (index + 1))
          : n
          ? prefix2 + (parseInt(recStore) + index)
          : prefix2 + parseInt(recStore);
        let str2 = y.substring(y.length - prefix2.length);
        let id = str + str2;
        await sqlRequest.query(Userdb.postAddStorage(addstorages[index], id));
        n = true;
      }
      console.log('log add', addstorages);
      resolve('OK');
    } catch (error) {
      next(error);
    }
  });
};

exports.addunits = (addun, comp, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let prefix = '000';
      let prefix2 = '0000';
      let n = false;
      let x = prefix + parseInt(comp.substring(2, 6));
      let str = x.substring(x.length - prefix.length);
      let dataId = await sqlRequest.query(Userdb.getRecentIdUnitByComp(str));
      let recUnit = dataId.recordset.length ? dataId.recordset[0].Unit_Id : '1';
      for (let index = 0; index < addun.length; index++) {
        let y = dataId.recordset.length
          ? prefix2 + (parseInt(recUnit.substring(5)) + (index + 1))
          : n
          ? prefix2 + (parseInt(recUnit) + index)
          : prefix2 + parseInt(recUnit);

        let str2 = y.substring(y.length - prefix2.length);
        let id = str + str2;
        await sqlRequest.query(Userdb.postAddUnit(addun[index], id, comp));
        n = true;
      }
      console.log('log add', addun);
      resolve('OK');
    } catch (error) {
      next(error);
    }
  });
};

exports.addcarrys = (materialsList, adminId, comp, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      // let carry = await sqlRequest.query(Userdb.getcarryId(materialsList[materialsList.length-1].Mat_Id));
      // let valid = false;
      // for (let i = 0; i < materialsList.length; i++) {
      //   let checkCarryIdAndMatId = await sqlRequest.query(
      //     Userdb.checkIdCarryAndMatId(
      //       carry.recordset[0].Carry_Id,
      //       materialsList[i].Mat_Id
      //     )
      //   );
      //   if (checkCarryIdAndMatId.recordset.length) {
      //     valid = true;
      //     break;
      //   }
      // }

      // if (!valid) {
      let prefix = '000';
      let prefix2 = '0000';
      let today = moment().format('YYYY-MM-DD');
      let x = prefix + parseInt(comp.substring(2, 6));
      let str = x.substring(x.length - prefix.length);
      let dataId = await sqlRequest.query(Userdb.getRecentCarryIdByComp(str));
      let recCarry = dataId.recordset.length
        ? dataId.recordset[0].Carry_Id
        : '1';
      for (let i = 0; i < materialsList.length; i++) {
        let y = !dataId.recordset.length
          ? prefix2 + parseInt(recCarry)
          : moment(dataId.recordset[0].Carry_Date).format('YYYY-MM-DD') ===
            today
          ? prefix2 + parseInt(recCarry.substring(5))
          : prefix2 + (parseInt(recCarry.substring(5)) + 1);
        let str2 = y.substring(y.length - prefix2.length);
        let id = str + str2;
        await sqlRequest.query(
          Userdb.postAddCarrys(
            materialsList[i].Mat_Id,
            materialsList[i].Mat_Num,
            adminId,
            id,
            today
          )
        );
      }
      if (materialsList.length) {
        resolve('OK');
      } else {
        const error = new Error('เลขเอกสารซ้ำหรือถูกบันทึกแล้ว');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.adduserfile = (excelData, adminid, comp, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let valid = false;
      for (let i = 0; i < excelData.length; i++) {
        let identity = await sqlRequest.query(
          Userdb.checkIdentification(excelData[i].cardnumber)
        );
        if (identity.recordset.length) {
          valid = true;
          break;
        }
      }

      if (!valid) {
        let prefix = '000';
        let prefix2 = '0000';
        n = false;
        let x = prefix + parseInt(comp.substring(2, 6));
        let str = x.substring(x.length - prefix.length);
        let dataId = await sqlRequest.query(Userdb.getRecentUserByComp(str));
        let recUserFile = dataId.recordset.length
          ? dataId.recordset[0].User_Id
          : '1';

        for (let index = 0; index < excelData.length; index++) {
          let y = dataId.recordset.length
            ? prefix2 + (parseInt(recUserFile.substring(5)) + (index + 1))
            : n
            ? prefix2 + (parseInt(recUserFile) + index)
            : prefix2 + parseInt(recUserFile);
          let str2 = y.substring(y.length - prefix2.length);
          let id = str + str2;

          let username = generator.generate({
            length: 6,
            numbers: true,
          });
          let password = generator.generate({
            length: 8,
            numbers: true,
          });
          console.log(`${index}`, excelData[index]);
          await sqlRequest.query(
            Userdb.postAddUserFile(
              excelData[index],
              username,
              password,
              adminid,
              id
            )
          );
          n = true;
        }
        resolve('OK');
      } else {
        const error = new Error('เลขบัตรประชาชนถูกใช้แล้ว');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.addproduct = (name, brand, color, desc, protid, unit, comp, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let prefix = '000';
      let prefix2 = '0000';
      let x = prefix + parseInt(comp.substring(2, 6));
      let str = x.substring(x.length - prefix.length);
      let dataId = await sqlRequest.query(Userdb.getRecentProductIdByComp(str));
      let recProduct = dataId.recordset.length
        ? dataId.recordset[0].Prod_Id
        : '1';
      let y = dataId.recordset.length
        ? prefix2 + (parseInt(recProduct.substring(5)) + 1)
        : prefix2 + parseInt(recProduct);
      let str2 = y.substring(y.length - prefix2.length);
      let id = str + str2;
      let data = await sqlRequest.query(
        Userdb.postAddProduct(name, brand, color, desc, protid, unit, id)
      );
      if (data.rowsAffected) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

(exports.addware = (name, desc, comp, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let prefix = '000';
      let prefix2 = '0000';
      //set prefix company
      let x = prefix + parseInt(comp.substring(2, 6));
      let str = x.substring(x.length - prefix.length);
      //Find WareHouse Id LIKE '__${id}____'
      let dataId = await sqlRequest.query(
        Userdb.getRecentIdWarehouseByComp(str)
      );
      let recWare = dataId.recordset.length ? dataId.recordset[0].Ware_Id : '1';
      //set prefix2 Warehouse
      let y = dataId.recordset.length
        ? prefix2 + (parseInt(recWare.substring(5)) + 1)
        : prefix2 + parseInt(recWare);
      let str2 = y.substring(y.length - prefix2.length);
      let id = str + str2;
      let data = await sqlRequest.query(
        Userdb.postAddWare(name, desc, id, comp)
      );
      if (data.rowsAffected) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
}),
  // Edit
  (exports.editunit = (unitid, name, desc, next) => {
    return new Promise(async (resolve) => {
      try {
        const pool = await poolConnect;
        const sqlRequest = pool.request();
        const data = await sqlRequest.query(
          Userdb.postEditUnit(unitid, name, desc)
        );
        if (data.rowsAffected) {
          resolve('OK');
        } else {
          const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
          error.statusCode = 401;
          throw error;
        }
      } catch (error) {
        next(error);
      }
    });
  });

exports.editstorage = (storeid, name, desc, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(
        Userdb.postEditStore(storeid, name, desc)
      );
      if (data.rowsAffected) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.edituser = (userid, name, surname, email, tel, dep, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(
        Userdb.postEditUser(userid, name, surname, email, tel, dep)
      );
      if (data.rowsAffected) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.editware = (wareid, name, desc, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(
        Userdb.postEditWare(wareid, name, desc)
      );
      if (data.rowsAffected) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.editproduct = (
  prodid,
  name,
  brand,
  color,
  desc,
  protid,
  unit,
  next
) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(
        Userdb.postEditProduct(prodid, name, brand, color, desc, protid, unit)
      );
      if (data.rowsAffected) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

// Delete
exports.deletestorageall = (arr, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      for (let index = 0; index < arr.length; index++) {
        await sqlRequest.query(
          Userdb.postDeleteStorageAll(arr[index].Store_Id)
        );
      }
      console.log('remove', arr);
      resolve('OK');
    } catch (error) {
      next(error);
    }
  });
};

exports.deletetuserall = (arr, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      for (let index = 0; index < arr.length; index++) {
        await sqlRequest.query(Userdb.postDeleteUserAll(arr[index].User_Id));
      }
      console.log('remove', arr);
      resolve('OK');
    } catch (error) {
      next(error);
    }
  });
};

exports.deletewareall = (arr, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      for (let index = 0; index < arr.length; index++) {
        await sqlRequest.query(Userdb.postDeleteWareall(arr[index].Ware_Id));
      }
      console.log('log add', arr);
      resolve('OK');
    } catch (error) {
      next(error);
    }
  });
};

exports.deleteproductall = (arr, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      for (let index = 0; index < arr.length; index++) {
        await sqlRequest.query(Userdb.postDeleteProductAll(arr[index].Prod_Id));
      }
      console.log('remove', arr);
      resolve('OK');
    } catch (error) {
      next(error);
    }
  });
};

exports.deleteunitall = (arr, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      for (let index = 0; index < arr.length; index++) {
        await sqlRequest.query(Userdb.postDeleteUnitAll(arr[index].Unit_Id));
      }
      console.log('remove', arr);
      resolve('OK');
    } catch (error) {
      next(error);
    }
  });
};

// Search
exports.searchproducts = (compid, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(Userdb.postSearchProduct(compid));
      if (data.recordset) {
        resolve(data.recordset);
      } else {
        const error = new Error('เกิดข้อผิดพลาด ไม่มีข้อมูลที่ท่านเลือก');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.searchidrecipt = (comp, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(Userdb.postSearchIdRecipt(comp));
      if (data.recordset) {
        resolve(data.recordset);
      } else {
        const error = new Error('เกิดข้อผิดพลาด ไม่มีข้อมูลที่ท่านเลือก');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.searchidreturn = (comp, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(Userdb.postSearchIdReturn(comp));
      if (data.recordset) {
        resolve(data.recordset);
      } else {
        const error = new Error('เกิดข้อผิดพลาด ไม่มีข้อมูลที่ท่านเลือก');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.searchidreveal = (comp, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(Userdb.postSearchIdReveal(comp));
      if (data.recordset) {
        resolve(data.recordset);
      } else {
        const error = new Error('เกิดข้อผิดพลาด ไม่มีข้อมูลที่ท่านเลือก');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.searchidcheck = (comp, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(Userdb.postSearchIdCheck(comp));
      if (data.recordset) {
        resolve(data.recordset);
      } else {
        const error = new Error('เกิดข้อผิดพลาด ไม่มีข้อมูลที่ท่านเลือก');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.searchidcarry = (comp, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(Userdb.postSearchIdCarry(comp));
      if (data.recordset) {
        resolve(data.recordset);
      } else {
        const error = new Error('เกิดข้อผิดพลาด ไม่มีข้อมูลที่ท่านเลือก');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.searchmaterials = (compid, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(Userdb.postSearchMaterial(compid));
      if (data.recordset) {
        resolve(data.recordset);
      } else {
        const error = new Error('เกิดข้อผิดพลาด ไม่มีข้อมูลที่ท่านเลือก');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.newidcarry = (compid, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(Userdb.postNewIdCarry(compid));
      if (data.recordset) {
        resolve(data.recordset);
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};
