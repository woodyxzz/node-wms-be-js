const jwt = require("jsonwebtoken");
const config = require("../configs/index");
exports.GetExample = (next) => {
  return new Promise(async (resolve) => {
    try {
      resolve("request api success");
    } catch (error) {
      next(error);
    }
  });
};
exports.LoginExample = (req, next) => {
  return new Promise(async (resolve) => {
    try {
      const { username, password } = req.body;

      if (username !== "admin" && password !== "123456") {
        const error = new Error("ชื่อผู้ใช้หรือรหัสผ่าน ไม่ถูกต้อง");
        error.statusCode = 401;
        throw error;
      }

      let data = {
        id: 1,
        name: `${username}-anon`,
        role: "Admin",
      };
      //creaate token jwt
      const token = await createToken(data);
      //decode exp
      const expiresIn = jwt.decode(token);
      const result = {
        access_token: token,
        expires_in: expiresIn.exp,
        token_type: "Bearer",
      };
      resolve(result);
    } catch (error) {
      next(error);
    }
  });
};

const createToken = (data) => {
  const token = jwt.sign(
    {
      id: data.id,
      role: data.role,
      name: data.name,
    },
    config.SECRETKEY,
    { expiresIn: "2h" }
  );
  return token;
};
