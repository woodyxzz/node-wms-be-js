const config = require('../configs');
const CheckDb = require('../sql/check');
const MaterialDb = require('../sql/material');
const poolConnect = config.SQLCONNECTION.connect();
const moment = require('moment');
config.SQLCONNECTION.on('error', (err) => {
  console.log(err);
});

exports.AddCheck = (matId, num, date, userId, compId, matNum, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let prefix = '000';
      let prefix2 = '0000';
      let today = moment().format('YYYY-MM-DD');
      let x = prefix + parseInt(compId.substring(2, 6));
      let str = x.substring(x.length - prefix.length);
      let dataId = await sqlRequest.query(CheckDb.getRecentCheckIdByComp(str));
      let rec = dataId.recordset.length ? dataId.recordset[0].Check_Id : '1';
      let y = !dataId.recordset.length
        ? prefix2 + parseInt(rec)
        : moment(dataId.recordset[0].Check_Date).format('YYYY-MM-DD') === today
        ? prefix2 + parseInt(rec.substring(5))
        : prefix2 + (parseInt(rec.substring(5)) + 1);
      let str2 = y.substring(y.length - prefix2.length);
      let id = str + str2;
      let data = await sqlRequest.query(
        CheckDb.addCheck(matId, num, date, userId, id, matNum)
      );
      if (data.rowsAffected[0] != 0) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.AddReturn = (matId, num, date, userId, compId, matNum, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let prefix = '000';
      let prefix2 = '0000';
      let today = moment().format('YYYY-MM-DD');
      let x = prefix + parseInt(compId.substring(2, 6));
      let str = x.substring(x.length - prefix.length);
      let dataId = await sqlRequest.query(CheckDb.getRecentReturnIdByComp(str));
      let rec = dataId.recordset.length ? dataId.recordset[0].Return_Id : '1';
      let y = !dataId.recordset.length
        ? prefix2 + parseInt(rec)
        : moment(dataId.recordset[0].Return_Date).format('YYYY-MM-DD') === today
        ? prefix2 + parseInt(rec.substring(5))
        : prefix2 + (parseInt(rec.substring(5)) + 1);
      let str2 = y.substring(y.length - prefix2.length);
      let id = str + str2;
      let sum = num + matNum;
      let data = await sqlRequest.query(
        CheckDb.addReturn(matId, num, date, userId, id, matNum)
      );
      let data2 = await sqlRequest.query(MaterialDb.updateNum(matId, sum));
      if (data.rowsAffected[0] != 0 && data2.rowsAffected[0] != 0) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.RecentInfoByMat = (matId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(CheckDb.getCheckInfoByMat(matId));
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('ไม่พบข้อมูล กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};
