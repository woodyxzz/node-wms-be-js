const config = require('../configs');
const UnitDb = require('../sql/unit');
const poolConnect = config.SQLCONNECTION.connect();
config.SQLCONNECTION.on('error', (err) => {
  console.log(err);
});

exports.ListByComp = (compId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(UnitDb.getListUnit(compId));
      if (data.recordset[0]) {
        resolve(data.recordset);
      } else {
        const error = new Error('ไม่พบข้อมูล กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};
