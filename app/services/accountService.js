const config = require('../configs');
const poolConnect = config.SQLCONNECTION.connect();
const AdminDb = require('../sql/Admin');
config.SQLCONNECTION.on('error', (err) => {
  console.log(err);
});

exports.Forgot = (email, username, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(
        AdminDb.getAdminForgot(username, email)
      );
      let data2 = await sqlRequest.query(
        AdminDb.getUserForgot(username, email)
      );
      if (data.recordset.length) {
        resolve(data.recordset);
      } else if (data2.recordset.length) {
        resolve(data2.recordset);
      } else {
        const error = new Error('ไม่พบผู้ใช้ในระบบ กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.ResetPass = (id, pass, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(AdminDb.resetPassById(id, pass));
      if (data.rowsAffected[0] != 0) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};
