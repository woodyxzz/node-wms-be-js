const config = require('../configs');

exports.Reject = (email, note, name, con) => {
  return new Promise(async (resolve) => {
    const mailOptions = {
      from: 'wutxwooddy@gmail.com',
      // to: email,
      to: 'wutxwooddy@gmail.com',
      subject: 'WMS: Notification',
      html: config.TEMPLATE.html(note, name, con),
    };
    config.TRANSPORTER.sendMail(mailOptions, function (err, info) {
      if (err) {
        console.log(err);
        resolve(err);
      } else {
        console.log(info);
        resolve('OK');
      }
    });
  });
};

exports.Remove = (email, note, name, con) => {
  return new Promise(async (resolve) => {
    const mailOptions = {
      from: 'wutxwooddy@gmail.com',
      // to: email,
      to: 'wutxwooddy@gmail.com',
      subject: 'WMS: Notification',
      html: config.TEMPLATE.html(note, name, con),
    };
    config.TRANSPORTER.sendMail(mailOptions, function (err, info) {
      if (err) {
        console.log(err);
        resolve(err);
      } else {
        console.log(info);
        resolve('OK');
      }
    });
  });
};

exports.Forgot = (email, name, con, pass) => {
  return new Promise(async (resolve) => {
    const mailOptions = {
      from: 'wutxwooddy@gmail.com',
      // to: email,
      to: 'wutxwooddy@gmail.com',
      subject: 'WMS: Notification',
      html: config.TEMPLATE.html(note, name, con, pass),
    };
    config.TRANSPORTER.sendMail(mailOptions, function (err, info) {
      if (err) {
        console.log(err);
        resolve(err);
      } else {
        console.log(info);
        resolve('OK');
      }
    });
  });
};
