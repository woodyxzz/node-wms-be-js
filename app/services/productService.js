const config = require('../configs');
const ProductDb = require('../sql/product');
const GoodsDb = require('../sql/goods');
const poolConnect = config.SQLCONNECTION.connect();
config.SQLCONNECTION.on('error', (err) => {
  console.log(err);
});

exports.CateList = (compId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(ProductDb.getCateList(compId));
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('ไม่พบข้อมูล กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.TypeList = (cateId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(ProductDb.getTypeList(cateId));
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('ไม่พบข้อมูล กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.TypeListByComp = (compId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(ProductDb.getTypeListByComp(compId));
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('ไม่พบข้อมูล กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.ListByType = (compId, typeId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(
        ProductDb.getListByType(compId, typeId)
      );
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.GoodsByPro = (proId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(GoodsDb.getGoodsByProd(proId));
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.DelGoods = (arr, proId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      for (let i = 0; i < arr.length; i++) {
        await sqlRequest.query(ProductDb.delGoods(arr[i].Mat_Id, proId));
      }
      if (arr.length) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.AddGoods = (arr, proId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      for (let i = 0; i < arr.length; i++) {
        await sqlRequest.query(ProductDb.addGoods(arr[i].matId, arr[i].num, proId));
      }
      if (arr.length) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.AddCate = (arr, compId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let prefix = '000';
      let prefix2 = '0000';
      let n = false;
      let x = prefix + parseInt(compId.substring(2, 6));
      let str = x.substring(x.length - prefix.length);
      let dataId = await sqlRequest.query(
        ProductDb.getRecentProCateIdByComp(str)
      );
      let rec = dataId.recordset.length
        ? dataId.recordset[0].Prod_Cate_Id
        : '1';
      for (let i = 0; i < arr.length; i++) {
        let y = dataId.recordset.length
          ? prefix2 + (parseInt(rec.substring(5)) + (i + 1))
          : n
          ? prefix2 + (parseInt(rec) + i)
          : prefix2 + parseInt(rec);
        let str2 = y.substring(y.length - prefix2.length);
        let id = str + str2;
        await sqlRequest.query(
          ProductDb.addCate(arr[i].name, arr[i].desc, id, compId)
        );
        n = true;
      }
      if (arr.length) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.AddType = (arr, compId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let prefix = '000';
      let prefix2 = '0000';
      let n = false;
      let x = prefix + parseInt(compId.substring(2, 6));
      let str = x.substring(x.length - prefix.length);
      let dataId = await sqlRequest.query(
        ProductDb.getRecentProTypeIdByComp(str)
      );
      let rec = dataId.recordset.length
        ? dataId.recordset[0].Prod_Type_Id
        : '1';
      for (let i = 0; i < arr.length; i++) {
        let y = dataId.recordset.length
          ? prefix2 + (parseInt(rec.substring(5)) + (i + 1))
          : n
          ? prefix2 + (parseInt(rec) + i)
          : prefix2 + parseInt(rec);
        let str2 = y.substring(y.length - prefix2.length);
        let id = str + str2;
        await sqlRequest.query(
          ProductDb.addType(arr[i].name, arr[i].desc, arr[i].cateId, id)
        );
        n = true;
      }
      if (arr.length) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.EditCate = (name, desc, cateId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(ProductDb.editCate(name, desc, cateId));
      if (data.rowsAffected[0] != 0) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.EditType = (name, desc, typeId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(ProductDb.editType(name, desc, typeId));
      if (data.rowsAffected[0] != 0) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.DeleteCate = (arr, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      for (let i = 0; i < arr.length; i++) {
        await sqlRequest.query(ProductDb.deleteCate(arr[i].Prod_Cate_Id));
      }
      if (arr.length) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.DeleteType = (arr, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      for (let i = 0; i < arr.length; i++) {
        await sqlRequest.query(ProductDb.deleteType(arr[i].Prod_Type_Id));
      }
      if (arr.length) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.ListByComp = (compId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(ProductDb.getListByComp(compId));
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};
