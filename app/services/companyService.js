const config = require('../configs');
const CompanyDb = require('../sql/company');
const poolConnect = config.SQLCONNECTION.connect();
config.SQLCONNECTION.on('error', (err) => {
  console.log(err);
});

exports.CompanyById = (compId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(CompanyDb.getCompanyById(compId));
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('ไม่พบข้อมูล กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};
