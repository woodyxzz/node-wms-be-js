const Userdb = {
  // List
  getListUser() {
    let queryString = `SELECT * FROM [User]`;
    return queryString;
  },
  getListWare() {
    let queryString = `SELECT * FROM [Warehouse]`;
    return queryString;
  },

  getListUnit() {
    let queryString = `SELECT * FROM [Unit]`;
    return queryString;
  },

  getListCountRec() {
    let queryString = `SELECT COUNT(Receive_Id) CountOfDate FROM [Receive]
    WHERE Receive_Date = CONVERT(varchar,GETDATE(),23) ;
    `;
    return queryString;
  },

  getListCountRev() {
    let queryString = `SELECT COUNT(Order_Id) CountOfDateRev FROM [Order]
    WHERE Order_Date = CONVERT(varchar,GETDATE(),23) AND Order_Status = 3 ;`;
    return queryString;
  },

  getListSelectUnit() {
    let queryString = `SELECT Unit_Id,Unit_Name FROM [Unit] `;
    return queryString;
  },

  postNameWare(idWare) {
    let queryString = `SELECT Ware_Name FROM [Warehouse] 
    WHERE  Ware_Id ='${idWare}';
    `;
    return queryString;
  },

  postListProfile(id) {
    let queryString = `SELECT Admin_Name FROM [Admin]
    WHERE Admin_Id = '${id}';`;
    return queryString;
  },
  postListStorage(idWare) {
    let queryString = `SELECT * FROM [Storage]
    WHERE  Ware_Id ='${idWare}';
    `;
    return queryString;
  },

  postShowProducts(typeid) {
    let queryString = `SELECT t1.Prod_Id, t1.Prod_Name, t1.Prod_Brand, t1.Prod_Color, t1.Prod_Desc, t1.Prod_Type_Id, t2.Unit_Id, t2.Unit_Name FROM [Product] t1
    LEFT JOIN Unit t2 ON t1.Unit_Id = t2.Unit_Id
    WHERE Prod_Type_Id = '${typeid}' `;
    return queryString;
  },

  postShowMaterials(typeid, typeidlast, comp) {
    let queryString = `SELECT t1.Mat_Id, t1.Mat_Name, t1.Mat_Price,t1.Mat_Num, t1.Mat_Min, t2.Unit_Id, t2.Unit_Name  ,t3.Comp_Name,t1.Mat_Num - t1.Mat_Min difference FROM Material t1
    INNER JOIN Unit t2 ON t1.Unit_Id = t2.Unit_Id
	  INNER JOIN Company t3 ON t2.Comp_Id = t3.Comp_Id
    WHERE t1.Mat_Id  BETWEEN '${typeid}' AND '${typeidlast}' AND t3.Comp_Id = '${comp}'  `;
    return queryString;
  },

  postShowCarryMaterials(matid, matidlast, comp) {
    let queryString = `SELECT t1.Mat_Id, t1.Mat_Num FROM Material t1
    INNER JOIN Unit t2 ON t1.Unit_Id = t2.Unit_Id
    INNER JOIN Company t3 ON t2.Comp_Id = t3.Comp_Id
    WHERE t1.Mat_Id  BETWEEN '${matid}' AND '${matidlast}' AND t3.Comp_Id = '${comp}'`;
    return queryString;
  },

  postShowReceiptInDate(recId, comp) {
    let queryString = `SELECT t1.Receive_Id , t1.Receive_Date, t1.Receive_Num, t2.User_Id, t2.User_Name, t1.Mat_Id, t5.Mat_Name, t5.Mat_Price, t4.Comp_Name, t5.Unit_Id, t6.Unit_Name, t1.Receive_Num * t5.Mat_Price remain FROM [Receive] t1
    INNER JOIN [User] t2 ON t1.User_Id = t2.User_Id
    INNER JOIN Admin t3 ON t2.Admin_Id = t3.Admin_Id
    INNER JOIN Company t4 ON t3.Comp_Id = t4.Comp_Id
    INNER JOIN Material t5 ON t1.Mat_Id = t5.Mat_Id
    INNER JOIN Unit t6 ON t5.Unit_Id = t6.Unit_Id
    WHERE t1.Receive_Id = '${recId}' AND  t3.Comp_Id = '${comp}'`;
    return queryString;
  },

  postShowIdReturn(retId, comp) {
    let queryString = `SELECT t1.Return_Id , t1.Return_Date, t1.Return_Num, t2.User_Id, t2.User_Name, t1.Mat_Id, t5.Mat_Name, t5.Mat_Price, t4.Comp_Name, t5.Unit_Id, t6.Unit_Name, t1.Return_Num * t5.Mat_Price remain FROM [Return] t1
    INNER JOIN [User] t2 ON t1.User_Id = t2.User_Id
    INNER JOIN Admin t3 ON t2.Admin_Id = t3.Admin_Id
    INNER JOIN Company t4 ON t3.Comp_Id = t4.Comp_Id
    INNER JOIN Material t5 ON t1.Mat_Id = t5.Mat_Id
    INNER JOIN Unit t6 ON t5.Unit_Id = t6.Unit_Id
    WHERE t1.Return_Id = '${retId}' AND  t3.Comp_Id = '${comp}'`;
    return queryString;
  },

  postSearchReveal(revId, comp) {
    let queryString = `SELECT t1.Order_Id , t1.Order_Date, t1.Order_Status , t1.User_Id, t2.User_Name, t5.Prod_Id, t5.List_Num, t4.Comp_Name, t6.Mat_Id, t7.Mat_Name, t7.Mat_Price, t7.Unit_Id, t8.Unit_Name, t5.List_Num * t7.Mat_Price remain FROM [Order] t1
    INNER JOIN [User] t2 ON t1.User_Id = t2.User_Id
    INNER JOIN Admin t3 ON t2.Admin_Id = t3.Admin_Id
    INNER JOIN Company t4 ON t3.Comp_Id = t4.Comp_Id
    INNER JOIN List t5 ON t1.Order_Id = t5.Order_Id
    INNER JOIN Goods t6 ON t5.Prod_Id = t6.Prod_Id
    INNER JOIN Material t7 ON t6.Mat_Id = t7.Mat_Id
    INNER JOIN Unit t8 ON t7.Unit_Id = t8.Unit_Id
    WHERE t1.Order_Id = '${revId}' AND t1.Order_Status = 2 AND t3.Comp_Id = '${comp}'`;
    return queryString;
  },

  postShowCheck(chId, comp) {
    let queryString = `SELECT t1.Check_Id , t1.Check_Date, t1.Check_Num, t1.Mat_Value, t2.User_Id, t2.User_Name, t1.Mat_Id, t5.Mat_Name, t4.Comp_Name, t5.Unit_Id, t6.Unit_Name, t1.Mat_Value - t1.Check_Num Lost , t1.Mat_Value + (t1.Check_Num - t1.Mat_Value) Remain FROM [Check] t1
    INNER JOIN [User] t2 ON t1.User_Id = t2.User_Id
    INNER JOIN Admin t3 ON t2.Admin_Id = t3.Admin_Id
    INNER JOIN Company t4 ON t3.Comp_Id = t4.Comp_Id
    INNER JOIN Material t5 ON t1.Mat_Id = t5.Mat_Id
    INNER JOIN Unit t6 ON t5.Unit_Id = t6.Unit_Id
    WHERE t1.Check_Id = '${chId}' AND t3.Comp_Id = '${comp}' AND t1.Check_Status = 1`;
    return queryString;
  },

  postSearchCarry(carId, comp) {
    let queryString = `SELECT t1.Carry_Id , t1.Carry_Date, t1.Carry_Num, t1.Admin_Id, t2.Admin_Name, t1.Mat_Id, t4.Mat_Name, t3.Comp_Name, t4.Unit_Id, t5.Unit_Name, t4.Store_Id FROM [Carry] t1
    INNER JOIN Admin t2 ON t1.Admin_Id = t2.Admin_Id
    INNER JOIN Company t3 ON t2.Comp_Id = t3.Comp_Id
    LEFT JOIN Material t4 ON t1.Mat_Id = t4.Mat_Id
    LEFT JOIN Unit t5 ON t4.Unit_Id = t5.Unit_Id
    LEFT JOIN Storage t6 ON t4.Store_Id = t6.Store_Id
    WHERE t1.Carry_Id = '${carId}' AND t3.Comp_Id = '${comp}' AND t1.Carry_Status = 1`;
    return queryString;
  },

  // Add
  postAddUser(
    username,
    password,
    name,
    surname,
    email,
    tel,
    userNo,
    dep,
    adminId,
    id
  ) {
    let queryString = `INSERT INTO [User] (User_Id, User_Username, User_Password, User_Name, User_Surname, User_Email, User_Tel, User_No,User_Dep, User_Status, Start_Date, Admin_Id)
    VALUES(
         'US' + '${id}'
         , '${username}'
         , '${password}'
         , '${name}'
         , '${surname}'
         , '${email}'
         , '${tel}'
         , '${userNo}'
         , ${dep}
         , 1
         , GETDATE()
		     , '${adminId}'
    ) 
    `;
    return queryString;
  },

  getRecentCarryIdByComp(str) {
    let queryString = `SELECT TOP 1 Carry_Id , Carry_Date FROM [Carry]
    WHERE Carry_Id LIKE '__${str}____'
    ORDER BY Carry_Id DESC
    `;
    return queryString;
  },

  getRecentUserByComp(str) {
    let queryString = `SELECT TOP 1 User_Id FROM [User]
    WHERE User_Id LIKE '__${str}____'
    ORDER BY User_Id DESC
    `;
    return queryString;
  },

  getRecentProductIdByComp(str) {
    let queryString = `SELECT TOP 1 Prod_Id FROM [Product]
    WHERE Prod_Id LIKE '__${str}____'
    ORDER BY Prod_Id DESC
    `;
    return queryString;
  },

  getRecentStoreIdByWare(str) {
    let queryString = `SELECT TOP 1 Store_Id FROM [Storage]
    WHERE Store_Id LIKE '__${str}____'
    ORDER BY Store_Id DESC
    `;
    return queryString;
  },

  getRecentIdUnitByComp(str) {
    let queryString = `SELECT TOP 1 Unit_Id FROM [Unit]
    WHERE Unit_Id LIKE '__${str}____'
    ORDER BY Unit_Id DESC
    `;
    return queryString;
  },
  getRecentIdWarehouseByComp(str) {
    let queryString = `SELECT TOP 1 Ware_Id FROM [Warehouse]
    WHERE Ware_Id LIKE '__${str}____'
    ORDER BY Ware_Id DESC
    `;
    return queryString;
  },

  postAddWare(name, desc, id, comp) {
    let queryString = `INSERT INTO [Warehouse] (Ware_Id,Ware_Name,Ware_Desc,Ware_Status,Comp_Id)
    VALUES(
        'WH' + '${id}'
        , '${name}'
         , '${desc}'
         , 1
         , '${comp}'
    ) 
    `;
    return queryString;
  },

  postAddProduct(name, brand, color, desc, protid, unit, id) {
    let queryString = `INSERT INTO [Product] (Prod_Id,Prod_Name,Prod_Brand,Prod_Color,Prod_Desc,Prod_Type_Id,Unit_Id)
    VALUES(
          'PD' + '${id}'
         , '${name}'
         , '${brand}'
         , '${color}'
         , '${desc}'
         , '${protid}'
         , '${unit}'
    ) 
    `;
    return queryString;
  },

  postAddStorage(item, id) {
    let queryString = `INSERT INTO [Storage] (Store_Id,Store_Name,Store_Desc,Store_Status,Ware_Id)
    VALUES (
      'ST'+ '${id}'
      ,'${item.Storagename}'
      ,'${item.Storagedesc}'
      ,1
      ,'${item.wareId}'
    )
      
    `;
    return queryString;
  },

  postAddUnit(item, id, comp) {
    let queryString = `INSERT INTO [Unit] (Unit_Id,Unit_Name,Unit_Desc,Comp_Id)
    VALUES(
      'UN'+ '${id}'
      ,'${item.unitname}'
      ,'${item.unitdesc}'
      ,'${comp}'
    )
    `;
    return queryString;
  },

  postAddCarrys(matId, matNum, adminId, id, today) {
    let queryString = `IF NOT EXISTS (SELECT Carry_Id FROM Carry WHERE Carry_Id = 'CA${id}' AND Mat_Id = '${matId}' AND Carry_Date = '${today}')
    BEGIN
         INSERT INTO Carry (Carry_Id, Carry_Num, Carry_Date, Carry_Status, Admin_Id, Mat_Id)
         VALUES ('CA${id}', ${matNum}, '${today}', 1, '${adminId}', '${matId}')
    END`;
    return queryString;
  },

  postAddUserFile(item, username, password, adminid, id) {
    let queryString = `INSERT INTO [User] (User_Id, User_Username, User_Password, User_Name, User_Surname,
      User_Email, User_Tel, User_No, User_Dep, User_Status, Start_Date, Admin_Id)
    VALUES(
      'US'+ '${id}'
      ,'${username}'
      ,'${password}'
      ,'${item.name}'
      ,'${item.surname}'
      ,'${item.email}'
      ,'${item.phonenumber}'
      ,'${item.cardnumber}'
      , ${item.dep}
      , 1
      , GETDATE()
      ,'${adminid}'
    )
    `;
    return queryString;
  },

  //Edit
  postEditUnit(unitid, name, desc) {
    let queryString = `UPDATE [Unit]
    SET Unit_Name = '${name}'
    , Unit_Desc = '${desc}'
    WHERE Unit_Id ='${unitid}';
    `;
    return queryString;
  },

  postEditStore(storeid, name, desc) {
    let queryString = `UPDATE [Storage]
    SET Store_Name = '${name}'
    , Store_Desc = '${desc}'
    WHERE Store_Id ='${storeid}';
    `;
    return queryString;
  },

  postEditUser(userid, name, surname, email, tel, dep) {
    let queryString = `UPDATE [User]
    SET User_Name = '${name}',
    User_Surname = '${surname}',
    User_Email = '${email}',
    User_Tel = '${tel}',
    User_Dep = '${dep}'
    WHERE  User_Id ='${userid}';
    `;
    return queryString;
  },

  postEditWare(wareid, name, desc) {
    let queryString = `UPDATE [Warehouse]
    SET Ware_Name = '${name}',
    Ware_Desc = '${desc}',
    Ware_Status = 1
    WHERE  Ware_Id ='${wareid}';
    `;
    return queryString;
  },

  postEditProduct(prodid, name, brand, color, desc, protid, unit) {
    let queryString = `UPDATE [Product]
    SET Prod_Name = '${name}',
    Prod_Brand = '${brand}',
    Prod_Color = '${color}',
    Prod_Desc = '${desc}',
    Prod_Type_Id = '${protid}',
    Unit_Id = '${unit}'
    WHERE  Prod_Id ='${prodid}';
    `;
    return queryString;
  },

  //Delete
  postDeleteUserAll(userid) {
    let queryString = `DELETE FROM [User] WHERE User_Id = '${userid}';
    `;
    return queryString;
  },

  postDeleteWareall(wareid) {
    let queryString = `DELETE FROM [Warehouse] WHERE Ware_Id = '${wareid}';
    `;
    return queryString;
  },

  postDeleteProductAll(prodid) {
    let queryString = `DELETE FROM [Product] WHERE Prod_Id = '${prodid}';
    `;
    return queryString;
  },

  postDeleteUnitAll(unitid) {
    let queryString = `DELETE FROM [Unit] WHERE Unit_Id = '${unitid}';
    `;
    return queryString;
  },

  postDeleteStorageAll(storeid) {
    let queryString = `DELETE FROM [Storage] WHERE Store_Id = '${storeid}';`;
    return queryString;
  },

  // Search
  postSearchProduct(compid) {
    let queryString = `SELECT t2.Prod_Type_Id, Prod_Type_Name FROM ProductCate t1
    INNER JOIN ProductType t2 ON t1.Prod_Cate_Id = t2.Prod_Cate_Id
    INNER JOIN Product t3 ON t2.Prod_Type_Id = t3.Prod_Type_Id
	  INNER JOIN Goods t4 ON t3.Prod_Id = t4.Prod_Id
	  INNER JOIN Material t5 ON t4.Mat_Id = t5.Mat_Id 
    INNER JOIN Storage t6 ON t5.Store_Id = t6.Store_Id
    INNER JOIN Warehouse t7 ON t6.Ware_Id = t7.Ware_Id
    INNER JOIN Company t8 ON t7.Comp_Id = t8.Comp_Id
    WHERE t7.Comp_Id = '${compid}' `;
    return queryString;
  },

  postSearchIdRecipt(comp) {
    let queryString = `SELECT DISTINCT  t1.Receive_Id, t1.Receive_Date FROM [Receive] t1
    INNER JOIN Material t2 ON t2.Mat_Id = t1.Mat_Id 
    INNER JOIN Storage t3 ON t3.Store_Id = t2.Store_Id
    INNER JOIN Warehouse t4 ON t4.Ware_Id = t3.Ware_Id
    INNER JOIN Company t5 ON t5.Comp_Id = t4.Comp_Id
    WHERE t5.Comp_Id = '${comp}' `;
    return queryString;
  },

  postSearchIdReturn(comp) {
    let queryString = `SELECT DISTINCT  t1.Return_Id, t1.Return_Date FROM [Return] t1
    INNER JOIN Material t2 ON t2.Mat_Id = t1.Mat_Id 
    INNER JOIN Storage t3 ON t3.Store_Id = t2.Store_Id
    INNER JOIN Warehouse t4 ON t4.Ware_Id = t3.Ware_Id
    INNER JOIN Company t5 ON t5.Comp_Id = t4.Comp_Id
    WHERE t5.Comp_Id = '${comp}'`;
    return queryString;
  },

  postSearchIdReveal(comp) {
    let queryString = `SELECT DISTINCT  t1.Order_Id, t1.Order_Date FROM [Order] t1
    INNER JOIN List t2 ON t1.Order_Id = t2.Order_Id
    INNER JOIN Goods t3 ON t2.Prod_Id = t3.Prod_Id
    INNER JOIN Material t4 ON t3.Mat_Id = t4.Mat_Id 
    INNER JOIN Storage t5 ON t4.Store_Id = t5.Store_Id
    INNER JOIN Warehouse t6 ON t5.Ware_Id = t6.Ware_Id
    INNER JOIN Company t7 ON t6.Comp_Id = t7.Comp_Id
    WHERE t7.Comp_Id = '${comp}' AND t1.Order_Status = 2
   `;
    return queryString;
  },

  postSearchIdCheck(comp) {
    let queryString = `SELECT DISTINCT  t1.Check_Id, t1.Check_Date FROM [Check] t1
    INNER JOIN Material t2 ON t2.Mat_Id = t1.Mat_Id 
    INNER JOIN Storage t3 ON t3.Store_Id = t2.Store_Id
    INNER JOIN Warehouse t4 ON t4.Ware_Id = t3.Ware_Id
    INNER JOIN Company t5 ON t5.Comp_Id = t4.Comp_Id
    WHERE t5.Comp_Id = '${comp}' AND t1.Check_Status = 1
   `;
    return queryString;
  },

  postSearchIdCarry(comp) {
    let queryString = `SELECT DISTINCT  t1.Carry_Id, t1.Carry_Date FROM [Carry] t1
    INNER JOIN Material t2 ON t2.Mat_Id = t1.Mat_Id 
    INNER JOIN Storage t3 ON t3.Store_Id = t2.Store_Id
    INNER JOIN Warehouse t4 ON t4.Ware_Id = t3.Ware_Id
    INNER JOIN Company t5 ON t5.Comp_Id = t4.Comp_Id
    WHERE t5.Comp_Id = '${comp}' AND t1.Carry_Status = 1
   `;
    return queryString;
  },

  postSearchMaterial(compid) {
    let queryString = `SELECT t1.Mat_Id, Mat_Name FROM Material t1
    INNER JOIN Storage t2 ON t1.Store_Id = t2.Store_Id
    INNER JOIN Warehouse t3 ON t2.Ware_Id = t3.Ware_Id
    INNER JOIN Company t4 ON t3.Comp_Id = t4.Comp_Id
    WHERE t4.Comp_Id = '${compid}' `;
    return queryString;
  },

  postNewIdCarry(compid) {
    let queryString = `SELECT TOP 1 t1.Carry_Id FROM [Carry] t1
    INNER JOIN Admin t2 ON t1.Admin_Id = t2.Admin_Id
    INNER JOIN Company t3 ON t2.Comp_Id = t3.Comp_Id
    WHERE t3.Comp_Id = '${compid}'
    ORDER BY (t1.Carry_Id) DESC`;
    return queryString;
  },

  checkIdUsername(username, userNo) {
    let queryString = `
    SELECT TOP 1 User_Username , User_No FROM [User]
    WHERE User_Username = '${username}' OR User_No = '${userNo}'
    `;
    return queryString;
  },

  checkIdAdmin(username) {
    let queryString = `
    SELECT TOP 1 Admin_Username FROM [Admin]
    WHERE Admin_Username = '${username}'
    `;
    return queryString;
  },

  checkIdentification(identity) {
    let queryString = `
    SELECT TOP 1 User_No FROM [User]
    WHERE User_No = ${identity}
    `;
    return queryString;
  },

  checkIdCarryAndMatId(carryid,matid){
    let queryString = ` 
    SELECT TOP 1 Carry_Id , Mat_Id FROM [Carry]
    WHERE Carry_Id = '${carryid}' AND Mat_Id = '${matid}'
    `;
    return queryString;
  },

  getcarryId(matid){
    let queryString = `
    SELECT TOP 1 Carry_Id FROM [Carry]
    WHERE Mat_Id  = '${matid}'
    ORDER BY Carry_Id DESC 
    `;
    return queryString;
  }
};

module.exports = Userdb;
