const ProductDb = {
  getCateList(compId) {
    // let queryString = `SELECT t1.Prod_Cate_Id, Prod_Cate_Name, Prod_Cate_Desc FROM ProductCate t1
    // LEFT JOIN ProductType t2 ON t1.Prod_Cate_Id = t2.Prod_Cate_Id
    // INNER JOIN Product t3 ON t2.Prod_Type_Id = t3.Prod_Type_Id
    // INNER JOIN Goods t4 ON t3.Prod_Id = t4.Prod_Id
    // INNER JOIN Material t5 ON t4.Mat_Id = t5.Mat_Id
    // INNER JOIN Storage t6 ON t5.Store_Id = t6.Store_Id
    // INNER JOIN Warehouse t7 ON t6.Ware_Id = t7.Ware_Id
    // INNER JOIN Company t8 ON t7.Comp_Id = t8.Comp_Id
    // WHERE t8.Comp_Id = '${compId}'`;
    let queryString = `SELECT Prod_Cate_Id, Prod_Cate_Name, Prod_Cate_Desc FROM ProductCate
    WHERE Comp_Id = '${compId}'`;
    return queryString;
  },
  getTypeList(cateId) {
    // let queryString = `SELECT t2.Prod_Type_Id, Prod_Type_Name, Prod_Type_Desc FROM ProductCate t1
    // RIGHT JOIN ProductType t2 ON t1.Prod_Cate_Id = t2.Prod_Cate_Id
    // INNER JOIN Product t3 ON t2.Prod_Type_Id = t3.Prod_Type_Id
    // INNER JOIN Goods t4 ON t3.Prod_Id = t4.Prod_Id
    // INNER JOIN Material t5 ON t4.Mat_Id = t5.Mat_Id
    // INNER JOIN Storage t6 ON t5.Store_Id = t6.Store_Id
    // INNER JOIN Warehouse t7 ON t6.Ware_Id = t7.Ware_Id
    // INNER JOIN Company t8 ON t7.Comp_Id = t8.Comp_Id
    // WHERE t1.Prod_Cate_Id = '${cateId}'`;
    let queryString = `SELECT Prod_Type_Id, Prod_Type_Name, Prod_Type_Desc FROM ProductType
    WHERE Prod_Cate_Id = '${cateId}'`;
    return queryString;
  },
  getTypeListByComp(compId) {
    let queryString = `SELECT t1.Prod_Type_Id, t1.Prod_Type_Name, t1.Prod_Type_Desc FROM ProductType t1
    INNER JOIN ProductCate t2 ON t1.Prod_Cate_Id = t2.Prod_Cate_Id
    WHERE t2.Comp_Id = '${compId}'`;
    return queryString;
  },
  getListByType(compId, typeId) {
    let queryString = `SELECT Prod_Id, Prod_Name, Prod_Brand, Prod_Color, Prod_Desc FROM Product
    WHERE Prod_Type_Id = '${typeId}'`;
    return queryString;
  },
  addCate(name, desc, id, compId) {
    let queryString = `INSERT INTO ProductCate (Prod_Cate_Id, Prod_Cate_Name, Prod_Cate_Desc, Comp_Id)
    VALUES ('PC${id}', '${name}', '${desc}', '${compId}')`;
    return queryString;
  },
  addType(name, desc, cateId, id) {
    let queryString = `INSERT INTO ProductType (Prod_Type_Id, Prod_Type_Name, Prod_Type_Desc, Prod_Cate_Id)
    VALUES ('PT${id}', '${name}', '${desc}', '${cateId}')`;
    return queryString;
  },
  addGoods(matId, num, proId) {
    let queryString = `IF EXISTS (SELECT Prod_Id FROM Goods WHERE Prod_Id = '${proId}' AND Mat_Id = '${matId}')
    BEGIN
        UPDATE Goods SET Goods_Num = ${num}
        WHERE Prod_Id = '${proId}' AND Mat_Id = '${matId}'
    END
    ELSE
    BEGIN
        INSERT INTO Goods (Prod_Id, Mat_Id, Goods_Num)
        VALUES ('${proId}', '${matId}', ${num})
    END`;
    return queryString;
  },
  editCate(name, desc, cateId) {
    let queryString = `UPDATE ProductCate
    SET Prod_Cate_Name = '${name}', Prod_Cate_Desc = '${desc}'
    WHERE Prod_Cate_Id = '${cateId}'`;
    return queryString;
  },
  editType(name, desc, typeId) {
    let queryString = `UPDATE ProductType
    SET Prod_Type_Name = '${name}', Prod_Type_Desc = '${desc}'
    WHERE Prod_Type_Id = '${typeId}'`;
    return queryString;
  },
  deleteCate(cateId) {
    let queryString = `DELETE FROM ProductCate
    WHERE Prod_Cate_Id = '${cateId}'`;
    return queryString;
  },
  deleteType(typeId) {
    let queryString = `DELETE FROM ProductType
    WHERE Prod_Type_Id = '${typeId}'`;
    return queryString;
  },
  delGoods(matId, proId) {
    let queryString = `DELETE FROM Goods
    WHERE Prod_Id = '${proId}' AND Mat_Id = '${matId}'`;
    return queryString;
  },
  getListByComp(compId) {
    let queryString = `SELECT DISTINCT t2.Prod_Id, t2.Prod_Name, t1.Prod_Type_Id, t1.Prod_Type_Name FROM ProductType t1
    INNER JOIN Product t2 ON t1.Prod_Type_Id = t2.Prod_Type_Id
    INNER JOIN Goods t3 ON t2.Prod_Id = t3.Prod_Id
    INNER JOIN Material t4 ON t3.Mat_Id = t4.Mat_Id
    INNER JOIN Storage t5 ON t4.Store_Id = t5.Store_Id
    INNER JOIN Warehouse t6 ON t5.Ware_Id = t6.Ware_Id
    WHERE t6.Comp_Id = '${compId}'`;
    return queryString;
  },
  getRecentProCateIdByComp(str) {
    let queryString = `SELECT TOP 1 Prod_Cate_Id FROM ProductCate
    WHERE Prod_Cate_Id LIKE '__${str}____'
    ORDER BY Prod_Cate_Id DESC`;
    return queryString;
  },
  getRecentProTypeIdByComp(str) {
    let queryString = `SELECT TOP 1 Prod_Type_Id FROM ProductType
    WHERE Prod_Type_Id LIKE '__${str}____'
    ORDER BY Prod_Type_Id DESC`;
    return queryString;
  },
};

module.exports = ProductDb;
