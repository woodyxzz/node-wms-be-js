const StorageDb = {
  getInfoByMat(matId) {
    let queryString = `SELECT t1.Store_Id, t1.Store_Name FROM Storage t1
    INNER JOIN Material t2 ON t1.Store_Id = t2.Store_Id
    WHERE t2.Mat_Id = '${matId}'`;
    return queryString;
  },
  getInfoByStore(store) {
    let queryString = `SELECT Store_Id, Store_Name FROM Storage
    WHERE Store_Id = '${store}'`;
    return queryString;
  },
};

module.exports = StorageDb;
