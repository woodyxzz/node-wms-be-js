const AdminDb = {
  getAdmin(compId) {
    let queryString = `SELECT Admin_Id, Admin_Name, Admin_Surname, Admin_Email, Admin_Tel, Admin_TaxNo, Admin_Status, Start_Date FROM Admin
    WHERE Comp_Id = '${compId}' AND Admin_Status = 1 OR Admin_Status = 2`;
    return queryString;
  },
  getUser(compId) {
    let queryString = `SELECT t1.User_Id, t1.User_Name, t1.User_Surname, t1.Start_Date, t1.User_Email, t1.User_Tel, t1.User_No, t1.User_Dep, t1.User_Status FROM [User] t1
    INNER JOIN Admin t2 ON t1.Admin_Id = t2.Admin_Id
    WHERE t2.Comp_Id = '${compId}' AND t1.User_Status = 1 OR t1.User_Status = 2`;
    return queryString;
  },
  getAdminForgot(username, email) {
    let queryString = `SELECT TOP 1 Admin_Id FROM Admin
    WHERE Admin_Username = '${username}' AND Admin_Email = '${email}'`;
    return queryString;
  },
  getUserForgot(username, email) {
    let queryString = `SELECT TOP 1 User_Id FROM [User]
    WHERE User_Username = '${username}' AND User_Email = '${email}'`;
    return queryString;
  },
  resetPassById(id, pass) {
    let queryString = `IF EXISTS (SELECT Admin_Id FROM Admin WHERE Admin_Id = '${id}')
    BEGIN
         UPDATE Admin SET Admin_Password = '${pass}'
         WHERE Admin_Id = '${id}'
    END
    ELSE
    BEGIN
         UPDATE [User] SET User_Password = '${pass}'
         WHERE User_Id = '${id}'
    END`;
    return queryString;
  },
  checkAdmin(username, password) {
    let queryString = `SELECT t1.Admin_Id, t1.Admin_Name, t1.Admin_Surname, t1.Admin_Status, t1.Comp_Id, t2.Comp_Status FROM Admin t1
    INNER JOIN Company t2 ON t1.Comp_Id = t2.Comp_Id
    WHERE Admin_Username = '${username}' AND Admin_Password = '${password}'`;
    return queryString;
  },
  checkSpAdmin(username, password) {
    let queryString = `SELECT * FROM [SpAdmin] WHERE SpAdmin_Username = '${username}' AND SpAdmin_Password = '${password}'`;
    return queryString;
  },
  // checkUser(username, password) {
  //   let queryString = `SELECT t1.User_Id, t1.User_Name, t1.User_Surname, t1.User_Status, t1.Admin_Id, t2.Comp_Id, t3.Comp_Status FROM [User] t1
  //   INNER JOIN Admin t2 ON t1.Admin_Id = t2.Admin_Id
  //   INNER JOIN Company t3 ON t2.Comp_Id = t3.Comp_Id
  //   WHERE t1.User_Username = '${username}' AND t1.User_Password = '${password}'`;
  //   return queryString;
  // },
  checkUser(username, password) {
    let queryString = `SELECT t1.User_Id, t1.User_Name, t1.User_Surname, t1.User_Dep, t1.Admin_Id, t2.Comp_Id, t3.Comp_Status FROM [User] t1
    INNER JOIN Admin t2 ON t1.Admin_Id = t2.Admin_Id
    INNER JOIN Company t3 ON t2.Comp_Id = t3.Comp_Id
    WHERE t1.User_Username = '${username}' AND t1.User_Password = '${password}'`;
    return queryString;
  },
  registerAdmin(company, companyAddress) {
    let queryString = `INSERT INTO Company (Comp_Id, Comp_Name, Comp_Address, Comp_Status, Comp_Max_Admin, Comp_Max_User)
        SELECT 
               'CO' + RIGHT('0000' + CAST(Comp_Id + 1 AS VARCHAR(4)), 4)
             , '${company}'
             , '${companyAddress}'
             , 0
             , 10
             , 30
        FROM (
             SELECT TOP 1 Comp_Id = CAST(RIGHT(Comp_Id, 4) AS INT)
             FROM Company
             ORDER BY Comp_Id DESC
        ) t`;
    return queryString;
  },
  getRecentCompanyId() {
    let queryString = `SELECT TOP 1 Comp_Id FROM Company ORDER BY Comp_Id DESC`;
    return queryString;
  },
  getRecentAdminId() {
    let queryString = `SELECT TOP 1 Admin_Id FROM Admin ORDER BY Admin_Id DESC`;
    return queryString;
  },
  getRecentAdminByComp(str) {
    let queryString = `SELECT TOP 1 Admin_Id FROM Admin
        WHERE Admin_Id LIKE '__${str}____'
        ORDER BY Admin_Id DESC`;
    return queryString;
  },
  CheckAdminByNo(headNo, username) {
    let queryString = `SELECT TOP 1 Admin_Id FROM Admin
        WHERE Admin_TaxNo = '${headNo}' OR Admin_Username ='${username}'`;
    return queryString;
  },
  CheckUserByNo(username) {
    let queryString = `SELECT TOP 1 User_Id FROM [User]
      WHERE User_Username = '${username}'`;
    return queryString;
  },
  registerAdmin2(
    id,
    username,
    password,
    email,
    name,
    surname,
    tel,
    headNo,
    compId
  ) {
    let queryString = `INSERT INTO Admin (Admin_Id, Admin_Username, Admin_Password, Admin_Name, Admin_Surname, Admin_Email, Admin_Tel, Admin_TaxNo, Admin_Status, Comp_Id)
        VALUES ('AD${id}', '${username}', '${password}', '${name}', '${surname}', '${email}', '${tel}', '${headNo}', 0, '${compId}')`;
    return queryString;
  },
  registerAdmin3(adminId, date) {
    let queryString = `INSERT INTO AdminManagement (Admin_Id, SpAdmin_Id, Register_Date)
        VALUES ('${adminId}', 'SA0000', '${date}')`;
    return queryString;
  },
  getCompany() {
    let queryString = `SELECT DISTINCT Comp_Id, Comp_Name, Comp_Address, Comp_Status, Comp_Max_Admin, Comp_Max_User FROM Company
        WHERE Comp_Status = 1 OR Comp_Status = 2`;
    return queryString;
  },
  getWaitCompany() {
    let queryString = `SELECT t1.Comp_Id, Comp_Name, t1.Comp_Address, t1.Comp_Status, t2.Admin_Id, t2.Admin_Name, t2.Admin_Surname, t2.Admin_Email, t2.Admin_Tel, t3.Register_Date FROM Company t1
    INNER JOIN Admin t2 ON t1.Comp_Id = t2.Comp_Id
    INNER JOIN AdminManagement t3 ON t2.Admin_Id = t3.Admin_Id
    WHERE t1.Comp_Status = 0 AND  t2.Admin_Status = 0`;
    return queryString;
  },
  editCompany(company, companyAddress, companyId) {
    let queryString = `UPDATE Company
        SET Comp_Name = '${company}', Comp_Address = '${companyAddress}'
        WHERE Comp_Id = '${companyId}'`;
    return queryString;
  },
  editAdmin(name, surname, tel, adminId) {
    let queryString = `UPDATE Admin
        SET Admin_Name = '${name}', Admin_Surname = '${surname}', Admin_Tel = '${tel}'
        WHERE Admin_Id = '${adminId}'`;
    return queryString;
  },
  editUser(name, surname, tel, userId) {
    let queryString = `UPDATE [User]
        SET User_Name = '${name}', User_Surname = '${surname}', User_Tel = '${tel}'
        WHERE User_Id = '${userId}'`;
    return queryString;
  },
  editAdminStatus(status, adminId) {
    let queryString = `UPDATE Admin
        SET Admin_Status = ${status}
        WHERE Admin_Id = '${adminId}'`;
    return queryString;
    //STATUS 0-3
  },
  editUserStatus(status, userId) {
    let queryString = `UPDATE [User]
        SET User_Status = ${status}
        WHERE User_Id = '${userId}'`;
    return queryString;
    //STATUS 0-3
  },
  editCompanyStatus(status, companyId) {
    let queryString = `UPDATE Company
        SET Comp_Status = ${status}
        WHERE Comp_Id = '${companyId}'`;
    return queryString;
    //STATUS 0-3
  },
  editCompanyMax(maxAdmin, maxUser, compId) {
    let queryString = `UPDATE Company
        SET Comp_Max_Admin = ${maxAdmin}, Comp_Max_User = ${maxUser}
        WHERE Comp_Id = '${compId}'`;
    return queryString;
  },
  getCountCompany() {
    let queryString = `SELECT COUNT(case when Comp_Status in (1, 2) then 1 end) AS count FROM Company`;
    return queryString;
  },
  getCountWaitCompany() {
    let queryString = `SELECT COUNT(case when Comp_Status in (0) then 1 end) AS count FROM Company`;
    return queryString;
  },
  getListWarehouse(adminId) {
    //BY ADMIN ID
    let queryString = `SELECT * FROM Warehouse
        LEFT JOIN Company ON Warehouse.Comp_Id = Company.Comp_Id
        INNER JOIN Admin ON Company.Comp_Id = Admin.Comp_Id
        WHERE Admin.Admin_Id ='${adminId}'`;
    return queryString;
  },
  getListStorage(ware) {
    //BY WAREHOUSE ID
    let queryString = `SELECT * FROM Storage
        WHERE Ware_Id ='${ware}'`;
    return queryString;
  },
  getListOldMaterial(adminId) {
    //BY ADMIN ID
    let queryString = `SELECT Mat_Id, Mat_Name FROM Material
        LEFT JOIN Storage ON Material.Store_Id = Storage.Store_Id
        INNER JOIN Warehouse ON Storage.Ware_Id = Warehouse.Ware_Id
        INNER JOIN Company ON Warehouse.Comp_Id = Company.Comp_Id
        INNER JOIN Admin ON Company.Comp_Id = Admin.Comp_Id
        WHERE Admin.Admin_Id ='${adminId}'`;
    return queryString;
  },
  getQrMaterial(name) {
    //BY MAT ID
    let queryString = `SELECT Qr_Id FROM Material
        WHERE Mat_Id ='${name}'`;
    return queryString;
  },
  getQrName(qrId) {
    //BY QR ID
    let queryString = `SELECT Qr_Name FROM QR
        WHERE Qr_Id ='${qrId}'`;
    return queryString;
  },
  addNewMaterial(name, store, num, price, unit, id) {
    let queryString = `INSERT INTO Material (Mat_Id, Mat_Name, Mat_Num, Mat_Min, Mat_Price, Unit_Id, Store_Id)
        VALUES ('MA${id}', '${name}', ${num}, 100, ${price}, '${unit}', '${store}')`;
    return queryString;
  },
  addNewSupplier(sup) {
    let queryString = `INSERT INTO Supplier (Sup_Id, Sup_Name, Sup_status)
        SELECT 
               'SP' + RIGHT('0000' + CAST(Sup_Id + 1 AS VARCHAR(4)), 4)
             , '${sup}'
             , 1
        FROM (
             SELECT TOP 1 Sup_Id = CAST(RIGHT(Sup_Id, 4) AS INT)
             FROM Supplier
             ORDER BY Sup_Id DESC
        ) t`;
    return queryString;
  },
  addNewProduction(matId, supId) {
    let queryString = `INSERT INTO Production (Sup_Id, Mat_Id)
        VALUES ('${supId}', '${matId}')`;
    return queryString;
  },
  addNewStorage(ware, store, num, date) {
    //TODO: INSERT INTO RECEIVE ?
    let queryString = `INSERT INTO Storage (Store_Id, Store_Name)
        SELECT 
               'ST' + RIGHT('0000' + CAST(Mat_Id + 1 AS VARCHAR(4)), 4)
             , '${name}'
        FROM (
             SELECT TOP 1 Mat_Id = CAST(RIGHT(Mat_Id, 4) AS INT)
             FROM Material
             ORDER BY Mat_Id DESC
        ) t`;
    return queryString;
  },
  addNewQr(qr) {
    let queryString = `INSERT INTO QR (Qr_Id, Qr_Name)
        SELECT 
               'QR' + RIGHT('0000' + CAST(Qr_Id + 1 AS VARCHAR(4)), 4)
             , '${qr}'
        FROM (
             SELECT TOP 1 Qr_Id = CAST(RIGHT(Qr_Id, 4) AS INT)
             FROM QR
             ORDER BY Qr_Id DESC
        ) t`;
    return queryString;
  },
  addNewMaterial2(mat, qrId) {
    let queryString = `UPDATE Material
        SET Qr_Id = '${qrId}'
        WHERE Mat_Id = '${mat}'`;
    return queryString;
  },
  getRecentMaterialId() {
    let queryString = `SELECT TOP 1 Mat_Id FROM Material ORDER BY Mat_Id DESC`;
    return queryString;
  },
  getRecentSupplierId() {
    let queryString = `SELECT TOP 1 Sup_Id FROM Supplier ORDER BY Sup_Id DESC`;
    return queryString;
  },
  getRecentQrId() {
    let queryString = `SELECT TOP 1 Qr_Id FROM QR ORDER BY Qr_Id DESC`;
    return queryString;
  },
};

module.exports = AdminDb;
