const GoodsDb = {
  getMatByProd(proId) {
    let queryString = `SELECT Mat_Id, Goods_Num FROM Goods
    WHERE Prod_Id = '${proId}'`;
    return queryString;
  },
  getGoodsByProd(proId) {
    let queryString = `SELECT t1.Mat_Id, t2.Mat_Name, t2.Mat_Color, t2.Mat_Size, t2.Mat_Price, t1.Goods_Num FROM Goods t1
    INNER JOIN Material t2 ON t1.Mat_Id = t2.Mat_Id
    WHERE t1.Prod_Id = '${proId}'`;
    return queryString;
  },
};

module.exports = GoodsDb;
