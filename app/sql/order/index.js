const OrderDb = {
  addOrder(date, userId, id) {
    let queryString = `INSERT INTO [Order] (Order_Id, Order_Date, Order_Status, User_Id)
    VALUES ('OR${id}', '${date}', 0, '${userId}')`;
    return queryString;
  },
  addOrder2(id, proId, num) {
    let queryString = `INSERT INTO List(Order_Id, Prod_Id, List_Num, List_Status)
    VALUES ('OR${id}', '${proId}', ${num}, 0)`;
    return queryString;
  },
  // getOrderByUser(userId) {
  //   let queryString = `SELECT t1.Order_Id, t1.Order_Date, t1.Order_Status, t1.Order_Num, t1.Prod_Id, t2.Prod_Name FROM [Order] t1
  //   INNER JOIN Product t2 ON t1.Prod_Id = t2.Prod_Id
  //   WHERE t1.User_Id = '${userId}'
  //   ORDER BY t1.Order_Status ASC, t1.Order_Date DESC`;
  //   return queryString;
  // },
  // getGoodsList(proId) {
  //   let queryString = `SELECT t1.Mat_Id, t2.Mat_Name, t3.Store_Name, t1.Goods_Num, t2.Mat_Num, t2.Mat_Min FROM Goods t1
  //   INNER JOIN Material t2 ON t1.Mat_Id = t2.Mat_Id
  //   INNER JOIN Storage t3 ON t2.Store_Id = t3.Store_Id
  //   WHERE Prod_Id = '${proId}'`;
  //   return queryString;
  // },
  getOrderByUser(userId) {
    let queryString = `SELECT Order_Id, Order_Date, Order_Status FROM [Order]
    WHERE User_Id = '${userId}'
    ORDER BY Order_Status ASC, Order_Date DESC`;
    return queryString;
  },
  getGoodsList(orderId) {
    let queryString = `SELECT t1.Prod_Id, t3.Prod_Name, SUM(t1.List_Num * t2.Goods_Num) AS Total_Num, t2.Mat_Id, t4.Mat_Name, t5.Store_Name, t4.Mat_Min, t6.Order_Date FROM List t1
    INNER JOIN Goods t2 ON t1.Prod_Id = t2.Prod_Id
    INNER JOIN Product t3 ON t2.Prod_Id = t3.Prod_Id
    INNER JOIN Material t4 ON t2.Mat_Id = t4.Mat_Id
    INNER JOIN Storage t5 ON t4.Store_Id = t5.Store_Id
    INNER JOIN [Order] t6 ON t1.Order_Id = t6.Order_Id
    WHERE t1.Order_Id = '${orderId}'
    GROUP BY t1.Prod_Id, t3.Prod_Name, t2.Mat_Id, t4.Mat_Name, t5.Store_Name, t4.Mat_Min, t6.Order_Date`;
    return queryString;
  },
  updateStatus(orderId, status) {
    let queryString = `UPDATE [Order]
    SET Order_Status = ${status}
    WHERE Order_Id = '${orderId}'`;
    return queryString;
  },
  getOrderByAdmin(adminId) {
    let queryString = `SELECT t1.Order_Id, t1.Order_Date, t1.Order_Status, t2.User_Name, t2.User_Surname FROM [Order] t1
    INNER JOIN [User] t2 ON t1.User_Id = t2.User_Id
    WHERE t1.Order_Status = 0 AND t2.Admin_Id = '${adminId}'`;
    return queryString;
  },
  getRecentOrderId() {
    let queryString = `SELECT TOP 1 Order_Id FROM [Order]
    ORDER BY Order_Id DESC`;
    return queryString;
  },
  getRecentOrderIdByComp(str) {
    let queryString = `SELECT TOP 1 Order_Id FROM [Order]
        WHERE Order_Id LIKE '__${str}____'
        ORDER BY Order_Id DESC`;
    return queryString;
  },
};

module.exports = OrderDb;
